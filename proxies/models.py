# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.validators import MinLengthValidator

# Create your models here.
class Proxies(models.Model):
    country = models.ForeignKey('Countries', on_delete=models.CASCADE)
    subnet = models.ForeignKey('Subnets', on_delete=models.CASCADE)
    channel = models.ForeignKey('Channels', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)

class Countries(models.Model):
    name = models.CharField(max_length=2, validators=[MinLengthValidator(2)], unique=True)

    def __str__(self):
        return self.name

class Subnets(models.Model):
    ip_address = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.ip_address

class Channels(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name

class Bulks(models.Model):
    country_name = models.CharField(max_length=2)
    subnet = models.CharField(max_length=50)
    channel_name = models.CharField(max_length=50)

from __future__ import unicode_literals
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db import transaction
from django.shortcuts import render
from .models import Proxies, Countries, Channels, Subnets, Bulks
import csv

from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.
def proxy_list(request):
    params = {
        "country" : request.GET.get('country', 'all'),
        "channel" : request.GET.get('channel', 'all'),
        "ip_address" : request.GET.get('ip_address', ''),
        "page" : request.GET.get('page', 1),
        "index" : (int(request.GET.get('page', 1)) - 1) * 100,
    }

    countries = Countries.objects.all().order_by('name')
    channels = Channels.objects.all().order_by('name')
    proxies_obj = Proxies.objects

    if params['country'] != 'all':
        proxies_obj = proxies_obj.filter(country_id=params['country'])

    if params['channel'] != 'all':
        proxies_obj = proxies_obj.filter(channel_id=params['channel'])

    if params['ip_address'] != '':
        proxies_obj = proxies_obj.filter(subnet__ip_address=params['ip_address'])

    proxies_obj = proxies_obj.all()

    temp = {}
    proxies_list = []

    for proxy in proxies_obj:
        if temp.get(proxy.subnet_id):
            temp[proxy.subnet_id]  += 1
            if temp[proxy.subnet_id] > 5:
                continue
        else:
            temp[proxy.subnet_id] = 1
        proxies_list.append(proxy)

    paginator = Paginator(proxies_list, 100)
    page_string = "country=" + params['country'] + "&channel=" + params['channel'] + "&ip_address=" + params['ip_address']

    try:
        proxies = paginator.page(params['page'])
    except PageNotAnInteger:
        proxies = paginator.page(1)
    except EmptyPage:
        proxies = paginator.page(paginator.num_pages)

    template = 'proxies/index.html'
    context = {
        "params" : params,
        "countries" : countries,
        "channels" : channels,
        "proxies" : proxies,
        "page_string" : page_string,
    }
    return render(request, template, context)

@transaction.atomic
def proxy_import(request):
    if request.method == 'POST':
        csv_file = request.FILES['csv_file']
        reader = csv.reader(csv_file, delimiter=str(u'\t').encode('utf-8'))
        try:
            for row in reader:
                try:
                    with transaction.atomic():
                        if len(row) != 3:
                            raise Exception('line %d has %d column(s)' % (reader.line_num, len(row)))

                        bulk = Bulks()
                        bulk.country_name = row[0]
                        bulk.subnet = row[1]
                        bulk.channel_name = row[2]
                        bulk.save()

                        country, country_created = Countries.objects.get_or_create(
                            name=bulk.country_name,
                        )
                        subnet, subnet_created = Subnets.objects.get_or_create(
                            ip_address=bulk.subnet,
                        )
                        channel, channel_created = Channels.objects.get_or_create(
                            name=bulk.channel_name,
                        )

                        proxy = Proxies()
                        proxy.country_id = country.id
                        proxy.subnet_id = subnet.id
                        proxy.channel_id = channel.id
                        proxy.save()

                except Exception as e:
                    transaction.set_rollback(True)
                    messages.error(request, e.message, extra_tags='html_safe alert alert-danger')
                    return HttpResponseRedirect('/')

        except csv.Error as e:
            sys.exit('file %s, line %d: %s' % (csv_file.name, reader.line_num, e))
            messages.error(request, 'file %s, line %d: %s' % (csv_file.name, reader.line_num, e), extra_tags='html_safe alert alert-danger')
            return HttpResponseRedirect('/')

    messages.success(request, 'Form submission successful', extra_tags='html_safe alert alert-success')
    return HttpResponseRedirect('/')

from django.conf.urls import url
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^$', views.proxy_list, name='proxy_list'),
    url(r'^import/', views.proxy_import, name='proxy_import'),
]

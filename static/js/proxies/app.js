$(function() {
  setTimeout(function() {
      $('.messages').fadeOut('slow');
  }, 5000);
});

function csvSubmit() {
  var fileName = $('#csv_file').val();

  if(fileName == '' || fileName === undefined) {
    alert('Select the file first');
    return false;
  }

  var ext = fileName.split('.').pop();

  if(ext === 'csv') {
    $('#proxy-frm-import').submit();
  }
  else {
    alert('Only CSV is allowed.');
  }
}
